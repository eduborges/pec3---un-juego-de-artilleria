Super Inferno Hotdog v2
=======================

Este proyecto es la PEC3 de la asignatura **Programación Unity 2D** del **Master en diseño y desarrollo de videojuegos** de la **Universitat Oberta de Catalunya**.

Es una imitación del nivel 1-1 de [Super Mario Bros](https://www.youtube.com/watch?v=PsC0zIhWNww). Además es la continuación de la PEC2 con una serie de mejoras y añadidos.

Mejoras y añadidos
------------------

**Partículas**:

- Fondo del menú principal (partículas de fuego ardiendo constante)
- Aplastar un enemigo cebolla (burst de estrellas que representan un fuerte golpe) 
- Sprint de personaje (partículas de levantar polvo)
- Choque de una bola de fuego (burst de llamas más pequeñas)

**IA**:

- Existe un enemigo con forma de perrito que lanza bolas de fuego del cielo al jugador cuando está cerca. 
- Reaparece en varios puntos del mapa.
- Existe un indicador de donde cae el ataque usando raycast y particleCollider.

**Checkpoints**:

- Existen puntos representados con una salchicha pinchada que al pasar el usuario por encima pasan la información a recordar a un agente estático.

**Disparar**:

- El jugador puede disparar bolas de juego que avanzan hacia donde está mirando y que rebotan al tocar el suelo. Solo puede haber 4 bolas en el mapa al mismo tiempo.
- Al tocar cualquier cosa que no sea suelo se destruye.
- Los enemigos cebolla son destruidos por estas bolas de fuego.
- Para obtener el poder de disparar hay que recoger un powerUp de una llama. El cuál solo sale cuando se posee el anterior powerUp.


How to play
------------

- Reach de flag at the end of the level before time ends
- Get the maximun number of points and coins before you reach the flag
- Dont fall and dont let enemys hit you

Controls
--------

**Jump** - ↑

**Horizontal movement** - ← →

**Sprint** - ⇪ _Shift_

**Shoot** - _Space_

Play [here](https://alu0100885613.github.io/SuperInfernoHotdogv2/)

Gameplay
--------

[![U2D](https://i.ytimg.com/vi/HoJuHTVuHkI/hqdefault.jpg)](https://www.youtube.com/watch?v=xEROAcwDocs "Super Inferno Hotdog")

_Click image to access Gameplay video_

Support
-------

mail: <eduborges@uoc.edu>

Author
------

[Eduardo Borges Fernández](https://gitlab.com/eduborges)

Credits
-------

**Music**:

- [Byzantine (loop)](https://opengameart.org/content/byzantine-loop) by [t4ngr4m](https://opengameart.org/users/t4ngr4m) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

License
-------

The project is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).