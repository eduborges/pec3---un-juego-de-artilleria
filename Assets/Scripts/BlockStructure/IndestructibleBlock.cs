﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que define las características del bloque tipo indestructible */

public class IndestructibleBlock : Block {

    /// <summary>
    /// Constructor por defecto
    /// </summary>
	public IndestructibleBlock() {
        aspect = Resources.Load<Sprite>("IndestructibleBlock");
        movable = false;
    }
}
