﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla el comportamiento de la bandera situada al final de los niveles */

public class FlagController : MonoBehaviour {

    /// <summary>
    /// Colliders divididos a lo alto de la bandera 
    /// </summary>
    public Collider2D[] colliders;

    /// <summary>
    /// Puntos que da chochar con cada collider respectivamente
    /// </summary>
    public int[] collidersPoints;

    /// <summary>
    /// Semáforo que evita el choque con múltiples colliders de la bandera
    /// </summary>
    bool triggerMutex = false;

    /// <summary>
    /// Controlador de la UI
    /// </summary>
    UIManager uiManager;


    // Inicialización
    void Start () {
        uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
	}
	
    /// <summary>
    /// Acciones a realizar cuando el jugador choca contra la bandera
    /// </summary>
    /// <param name="col">Detalles del a colisión</param>
    void OnCollisionEnter2D(Collision2D col) {
        
        if (col.transform.tag == "Player") {
            if (!triggerMutex) {
                GameObject.Find("InGameSong").GetComponent<AudioSource>().Stop();                           // Parar la música de partida
                AudioAgent.victory.GetComponent<AudioSource>().Play();                                      // Reproducir música de victoria
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().changeControl();  // El código obtiene el control del personaje para entrar en el castillo
                triggerMutex = true;
                for (int i = 0; i < colliders.Length; i++) {        // Calcular puntos ganados dependiendo con la parte de la bandera con la que se ha chocado
                    if(col.otherCollider == colliders[i]) {
                        uiManager.AddPoints(collidersPoints[i]);
                        i = colliders.Length;
                    }
                }
                dropFlag();
            }
        }
    }

    /// <summary>
    /// Baja la bandera hasta el suelo
    /// </summary>
    void dropFlag() {
        // El hijo del palo de la bandera es la tela de la bandera
        transform.GetChild(0).GetComponent<Rigidbody2D>().isKinematic = false;
        foreach (Collider2D collider in colliders) {
            Destroy(collider);
        }
    }

}
