﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla la colisión de las partículas de las bolas de fuego enemigas*/
public class ParticleCollisionController : MonoBehaviour {

    /// <summary>
    /// Partículas emitidas al chocar una bola de fuego que lanza el enemigo perrito
    /// </summary>
    public ParticleSystem destroyParticles;

    /// <summary>
    /// Punto exacto donde se destruye si toca el suelo (tilemap)
    /// </summary>
    public Vector2 destroyPoint;

    /// <summary>
    /// Si la bola de fuego choca contra el jugador lo mata, si no, simplemente se destruye
    /// </summary>
    /// <param name="other">El objeto contra el que choca</param>
	void OnParticleCollision(GameObject other) {

        AudioAgent.fireballEnemy.GetComponent<AudioSource>().Play();
        ParticleSystem ps;
        ps = Instantiate(destroyParticles);
        if (other.transform.tag == "Player") {
            PlayerController pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            ps.transform.position = pc.transform.position;
            pc.die();
            
        } else if(other.transform.GetComponent<BlockController>()){
            ps.transform.position = other.transform.position;
        } else {
            ps.transform.position = destroyPoint;
        }
        ps.Emit(10);
    }
}
