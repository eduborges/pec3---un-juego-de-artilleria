﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerController : MonoBehaviour {

    /// <summary>
    /// Puntos que otorga al recogerse
    /// </summary>
    [SerializeField]
    int pointsValue = 1000;

    /// <summary>
    /// Controlador del jugador
    /// </summary>
    PlayerController playerController;

    /// <summary>
    /// Controlador de la interfaz
    /// </summary>
    UIManager uiManager;

    /// <summary>
    /// Semáforo que indica cuando ha sido obtenido el powerUp
    /// </summary>
    bool mutexAlreadyTaken;

    /// <summary>
    /// Tiempo entre cada cambio de color del powerup
    /// </summary>
    public float colorDeltaTime = 0.5f;

    /// <summary>
    /// Colores por los que va a transitar el powerup
    /// </summary>
    public Color32[] colorsTransition;

    /// <summary>
    /// Índice para seleccionar el color con el que un powerUp empieza a transitar en colorsTransition
    /// </summary>
    public int indexColor = 0;

    // Inicialización
    void Start() {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
        AudioAgent.powerUpAppears.GetComponent<AudioSource>().Play();
        mutexAlreadyTaken = false;

        InvokeRepeating("changeColor", colorDeltaTime, colorDeltaTime);
    }

    /// <summary>
    /// Aparición del powerUp
    /// </summary>
    /// <returns>IEnumerator</returns>
    public IEnumerator popUp() {
        float posY = 0.025f;

        for (int i = 0; i < 40; i++){
            transform.position = new Vector3(transform.position.x, transform.position.y + posY, transform.position.z);
            yield return new WaitForSeconds(0.025f);
        }

        GetComponent<BoxCollider2D>().enabled = true;
    }

    /// <summary>
    /// Acciones a realizar cuando detecta un trigger
    /// </summary>
    /// <param name="col">Detalles del collider</param>
    void OnTriggerEnter2D(Collider2D col){

        if (col.transform.tag == "Player" && !mutexAlreadyTaken) {
            flowerTaken();
        }
    }

    /// <summary>
    /// Aplica al jugador el powerUp y añade los puntos que vale
    /// </summary>
    private void flowerTaken() {
        AudioAgent.powerUpTaken.GetComponent<AudioSource>().Play();
        mutexAlreadyTaken = true;
        uiManager.AddPoints(pointsValue);
        playerController.pickUpPowerUp();
        Destroy(gameObject);
    }

    /// <summary>
    /// Cambios de color que se producen con el tiempo en el powerup
    /// </summary>
    void changeColor() {
        GetComponent<SpriteRenderer>().color = colorsTransition[indexColor];
        indexColor++;
        indexColor = indexColor % colorsTransition.Length;
    }
}
