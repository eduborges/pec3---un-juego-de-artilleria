﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla el comportamiento de las bolas de fuego lanzadas por el jugador */

public class FireballController : MonoBehaviour {

    /// <summary>
    /// Sentidos que pueden seguir las bolas
    /// </summary>
    public enum Direction:int { Negative = -1, Positive = 1}
    /// <summary>
    /// Sentido actual que siguen las bolas de fuego
    /// </summary>
    public static Direction currentDirection = Direction.Negative;

    /// <summary>
    /// Velocidad en el eje x de las bolas al aparecer
    /// </summary>
    [SerializeField]
    float _xAxisVelocity;

    /// <summary>
    /// Velocidad de rebote de las bolas (eje Y)
    /// </summary>
    [SerializeField]
    float _yAxisVelocity;

    /// <summary>
    /// Está bola ya ha chocado y ha sido destruida?
    /// </summary>
    bool destroyed = false;

    /// <summary>
    /// Efecto de partículas al destruirse una bola
    /// </summary>
    public ParticleSystem particlesDestroyEffect;

    public float xAxisVelocity {
        get { return _xAxisVelocity; }
        set { _xAxisVelocity = value; }
    }

    public float yAxisVelocity {
        get { return _yAxisVelocity; }
        set { _yAxisVelocity = value; }
    }
	
    void Start() {
        xAxisVelocity = xAxisVelocity * (int)currentDirection;
    }

	// Update is called once per frame
	void Update () {
        if(GetComponent<Rigidbody2D>())
            GetComponent<Rigidbody2D>().velocity = new Vector2(xAxisVelocity, GetComponent<Rigidbody2D>().velocity.y);
    }

    /// <summary>
    /// Si choca contra el suelo renueva velocidad, si no, es destruida
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter2D(Collision2D col) {
        if(col.transform.tag == "floor") {
            GetComponent<Rigidbody2D>().velocity = new Vector2(xAxisVelocity, yAxisVelocity);
        } else if(!destroyed){
            destroyed = true;
            StartCoroutine(DestroyEffect());       
        }
    }

    /// <summary>
    /// Efecto de destrucción de las bolas de fuego del personaje
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator DestroyEffect() {
        ParticleSystem ps = Instantiate(particlesDestroyEffect, GameObject.Find("FireballParticlesContainer").transform);
        ps.transform.position = gameObject.transform.position;
        ps.Emit(Random.Range(8, 9));
        DestroyTangibility();
        yield return new WaitForSeconds(ps.main.duration);
        Destroy(ps.gameObject);
        Destroy(gameObject);
    }

    /// <summary>
    /// Destruye visualmente las bolas de fuego
    /// </summary>
    void DestroyTangibility() {
        Destroy(GetComponent<SpriteRenderer>());
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<CircleCollider2D>());
    }
}
