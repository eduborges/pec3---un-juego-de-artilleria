﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointAgent : MonoBehaviour {

    /// <summary>
    /// Datos a guardar
    /// </summary>
    public struct CheckpointScoreData{
        public int points;
        public int coins;
        public float timeleft;
        public Vector3 respawnPoint;
    }

    /// <summary>
    /// Información a recordar del ultimo checkpoint alcanzado
    /// </summary>
    public static CheckpointScoreData cpData;
    /// <summary>
    /// Tiene datos que cargar de un checkpoint?
    /// </summary>
    static bool _hasCpData = false;
    /// <summary>
    /// Fue algún checkpoint cargado?
    /// </summary>
    static bool _checkPointWasLoaded = false;

    public static bool hasCpData {
        get { return _hasCpData; }
        set { _hasCpData = value; }
    }

    public static bool checkPointWasLoaded {
        get { return _checkPointWasLoaded; }
        set { _checkPointWasLoaded = value; }
    }

    /// <summary>
    /// Guarda la información de un checkpoint
    /// </summary>
    /// <param name="position">posición del checkpoint</param>
    public void saveUIData(Vector3 position) {
        hasCpData = true;
        UIManager uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
        cpData.points = uiManager.currentScore;
        cpData.coins = uiManager.currentCoins;
        cpData.timeleft = uiManager.MapTime < 20 ? 25 : uiManager.MapTime;
        cpData.respawnPoint = position;
    }

    /// <summary>
    /// Reinicia los checkpoints e invalida toda la información anterior
    /// </summary>
    public static void resetCheckpoints() {
        hasCpData = false;
    }

}
