﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Controlador de cada punto de control del nivel */

public class CheckPointController : MonoBehaviour {

    /// <summary>
    /// Agente que almacena y recibe la información de los checkpoints
    /// </summary>
    CheckpointAgent cpAgent;
    /// <summary>
    /// Ha sido ya activado el checkpoint?
    /// </summary>
    bool used = false;

    void Start() {
        cpAgent = GameObject.Find("CheckpointAgent").GetComponent<CheckpointAgent>(); 
    }

    /// <summary>
    /// Guarda la inforación del checkpoint cuando el jugador pasa por encima
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter2D(Collider2D col) {

        if (col.transform.tag == "Player" && !used) {
            used = true;
            activeEffect();

            //Si este checkpoint ha sido cargado, se ignora el guardado
            if (!CheckpointAgent.checkPointWasLoaded) {
                cpAgent.saveUIData(transform.position);
                AudioAgent.checkpoint.GetComponent<AudioSource>().Play();
            }
            CheckpointAgent.checkPointWasLoaded = false;

        }
    }

    /// <summary>
    /// Efecto visual de activación del checkpoint, la salchicha se pone roja
    /// </summary>
    void activeEffect() {
        Transform sausageIndicator = transform.GetChild(0);
        sausageIndicator.GetComponent<SpriteRenderer>().color = new Color32(193, 0, 0, 255);
    }
}

