﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

/* Clase que centraliza los cambios entre escenas */

public class SceneAgent : MonoBehaviour {

    public static void newGame() {
        SceneManager.LoadScene("GameScene");
    }

    public void returnMenu() {
        SceneManager.LoadScene("GameMenu");
    }

    /// <summary>
    /// Volver al menu principal desde el de pausa, por lo que el timeScale vuelve a ser 1f y las rondas se resetean.
    /// </summary>
    public void returnMenuFromPause(){
        GameplayManager.resetRounds();
        CheckpointAgent.resetCheckpoints();
        Time.timeScale = 1f;
        SceneManager.LoadScene("GameMenu");
    }

    public static void exitGame() {
        Application.Quit();
    }

    /// <summary>
    /// Recarga la escena actual
    /// </summary>
    public static void reloadScene(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public static void victoryScreen() {
        SceneManager.LoadScene("VictoryScene");
    }
}
