﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona el powerUp contenido en los bloques*/
public class ContentAssistant : MonoBehaviour {

    /// <summary>
    /// Posibles contenidos
    /// </summary>
    public GameObject[] dynamicContents;

    /// <summary>
    /// Devuelve el powerup que debería estar en los bloques para el poder actual del personaje
    /// </summary>
    /// <param name="playerCurrentPower">Poder actual del personaje</param>
    /// <returns>PowerUp que debe contener el bloque</returns>
    public GameObject giveRequiredContent(PlayerController.PlayerPower playerCurrentPower) {
        if((int)playerCurrentPower >= dynamicContents.Length) {
            return dynamicContents[dynamicContents.Length - 1];
        } else {
            return dynamicContents[(int)playerCurrentPower];
        }
    }
}
