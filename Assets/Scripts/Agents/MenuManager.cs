﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

/* Controlador del menu principal */

public class MenuManager : MonoBehaviour {
    
    /// <summary>
    /// Botón bloque actual alcanzado por el raycast
    /// </summary>
    GameObject blockRaycasted;

	// Lanzar raycast a cada frame
	void Update () {
        raycastMouse();
    }

    /// <summary>
    /// Hace raycast en la posición del cursor para poder interactuar con los botones bloque que no se encuentran en la UI
    /// </summary>
    void raycastMouse() {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero, 0f);

        if (hit) {
            if (blockRaycasted != hit.transform.gameObject){
                blockRaycasted = hit.transform.gameObject;
                blockRaycasted.GetComponent<BrickButtonController>().mouseOver(true);
            }
        } else {
            if (blockRaycasted) {
                blockRaycasted.GetComponent<BrickButtonController>().mouseOver(false);
                blockRaycasted = null;
            }
        }
    }
}
