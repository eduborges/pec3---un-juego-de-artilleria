﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona la interfaz y puntuaciones de la partida */

public class UIManager : MonoBehaviour {

    /// <summary>
    /// Display en el que se reflejan los puntos conseguidos del nivel
    /// </summary>
    public GameObject displayPoints;

    /// <summary>
    /// Display en el que se reflejan las monedas conseguidas del nivel
    /// </summary>
    public GameObject displayCoins;

    /// <summary>
    /// Display en el que se refleja el timer del nivel
    /// </summary>
    public GameObject displayTime;

    /// <summary>
    /// Puntos actuales
    /// </summary>
    int _currentScore;

    /// <summary>
    /// Monedas conseguidas actualmente
    /// </summary>
    int _currentCoins;

    /// <summary>
    /// Tiempo para completar el nivel
    /// </summary>
    [SerializeField]
    float _MapTime;

    //Propiedades

    public int currentScore {
        get { return _currentScore; }
        set { _currentScore = value; }
    }

    public int currentCoins {
        get { return _currentCoins; }
        set { _currentCoins = value; }
    }

    public float MapTime {
        get { return _MapTime; }
        set { _MapTime = value; }
    }

    // Inicialización
    void Start () {
        displayPoints.GetComponent<Text>().text = currentScore.ToString();
        displayCoins.GetComponent<Text>().text = currentCoins.ToString();
        displayTime.GetComponent<Text>().text = MapTime.ToString();
        currentScore = 0;
        currentCoins = 0;
    }

    /// <summary>
    /// Actualiza el valor del tiempo y comprueba si ha terminado
    /// </summary>
    void UpdateTime() {
        MapTime--;
        displayTime.GetComponent<Text>().text = MapTime.ToString();
        if(MapTime <= 0) {
            CancelInvoke("UpdateTime");
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().die();
        }
    }
    
    /// <summary>
    /// Añade puntos conseguidores
    /// </summary>
    /// <param name="points">Cantidad de puntos</param>
    public void AddPoints(int points){
        currentScore += points;
        displayPoints.GetComponent<Text>().text = currentScore.ToString();
    }

    /// <summary>
    /// Añade 1 moneda conseguida
    /// </summary>
    public void AddCoin() {
        currentCoins++;
        displayCoins.GetComponent<Text>().text = currentCoins.ToString();
    }

    public void refreshDisplays() {
        displayCoins.GetComponent<Text>().text = currentCoins.ToString();
        displayPoints.GetComponent<Text>().text = currentScore.ToString();
        displayTime.GetComponent<Text>().text = MapTime.ToString();
    }

    /// <summary>
    /// Inicia la cuentra atrás del timer
    /// </summary>
    public void startTiming() {
        InvokeRepeating("UpdateTime", 1.0f, 1.0f);
    }
}
