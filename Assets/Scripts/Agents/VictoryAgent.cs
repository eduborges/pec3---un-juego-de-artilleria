﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona las estadísticas al completar un nivel  */

public class VictoryAgent : MonoBehaviour {

    /// <summary>
    /// Monedas conseguidas
    /// </summary>
    public static int coins;

    /// <summary>
    /// Puntos conseguidos
    /// </summary>
    public static int chocopoints;

    /// <summary>
    /// Display de monedas conseguidas
    /// </summary>
    public GameObject coinsDisplay;

    /// <summary>
    /// Display de puntos conseguidos
    /// </summary>
    public GameObject chocopointsDisplay;

    // Lanza el recuento de estadísticas
    void Start () {
        StartCoroutine("displayCoinsScore");
        StartCoroutine("displayChocopointsScore");
    }

    /// <summary>
    /// Recuenta las monedas conseguidas 
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator displayCoinsScore() {

        for(int i = 0; i < coins; i++) {
            coinsDisplay.GetComponent<Text>().text = i.ToString();
            yield return new WaitForSeconds(0.2f);
        }
    }

    /// <summary>
    /// Recuenta los puntos conseguidos
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator displayChocopointsScore() {

        for (int i = 0; i < chocopoints; i = i + 10) {
            chocopointsDisplay.GetComponent<Text>().text = i.ToString();
            yield return new WaitForSeconds(0.0001f);
        }
    }
}
