﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttacks : MonoBehaviour {

    /// <summary>
    /// Objeto prefab que dispara el personaje
    /// </summary>
    public GameObject ammo;

    /// <summary>
    /// Script que controla como se desarrollan las acciones
    /// </summary>
    private PlayerController pController;

    // Use this for initialization
    void Start () {
        pController = GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space) && canIAttack()) {
            pController.Shoot(ammo);
            AudioAgent.fireballPlayer.GetComponent<AudioSource>().Play();
        }
    }
    
    /// <summary>
    /// Comprueba si se puede disparar
    /// </summary>
    /// <returns></returns>
    bool canIAttack() {
        return (pController.currentPower == PlayerController.PlayerPower.AmmoMode && GameObject.FindGameObjectsWithTag("hellfire").Length < 5) ? true : false;
    }
}
