﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona la entrada del usuario para el moviemiento del personaje */

public class PlayerMovement : MonoBehaviour {

    /// <summary>
    /// Entrada con la que se realiza el salto
    /// </summary>
    [SerializeField]
    public string jumpKey = "up";

    /// <summary>
    /// Valor del eje horizontal
    /// </summary>
    float horizontalAxis;

    /// <summary>
    /// Posibles estados del salto
    /// </summary>
    public enum JumpState { StartJump, Jumping, StopJumping, NotJumping};

    /// <summary>
    /// Estado actual del salto
    /// </summary>
    private JumpState jumpState;

    /// <summary>
    /// Controlador del jugador al que pasarle la información
    /// </summary>
    private PlayerController pController;

    /// <summary>
    /// Varible que permite sincronizar el principio del salto entre Update y FixedUpdate
    /// </summary>
    public static bool KeyDownSync = false;

    /// <summary>
    /// Varible que permite sincronizar el final del salto entre Update y FixedUpdate
    /// </summary>
    public static bool KeyUpSync = false;

    /// <summary>
    /// Posibles entidades que tienen el control del movimiento del personaje
    /// </summary>
    public enum HaveControl { User, Code};

    /// <summary>
    /// Entidad actual que controla el movimiento del personaje
    /// </summary>
    HaveControl whoHaveControl = HaveControl.User;

    // Preinicialización
    void Awake () {
        pController = GetComponent<PlayerController>();
        jumpState = JumpState.NotJumping;
    }
    
    // A cada frame actualziará los valores del movimiento dependiendo de quien tenga el control
	void Update() {
        if (whoHaveControl == HaveControl.User) {
            horizontalAxis = Input.GetAxisRaw("Horizontal");
            setJumpValue();
            checkSprint();
        } else {
            horizontalAxis = 1f;
        }
    }
 
    // A cada llamada de las físicas se actualiza el movimiento y el salto
	void FixedUpdate () {
        pController.Move(horizontalAxis);
        pController.Jump(jumpState);
    }

    /// <summary>
    /// Actualiza el estado del salto dependiendo del Input del usuario
    /// </summary>
    void setJumpValue() {
        if (Input.GetKeyDown(jumpKey)) {
            jumpState = JumpState.StartJump;
            KeyDownSync = true;             
        }
        else if (Input.GetKey(jumpKey)) {
            jumpState = JumpState.Jumping;
        }
        else if (Input.GetKeyUp(jumpKey)) {
            jumpState = JumpState.StopJumping;
            KeyUpSync = true;
        } else {
            jumpState = JumpState.NotJumping;
        }

        if (KeyDownSync && GetComponent<PlayerController>().grounded) // Hasta que se ha iniciado el salto, no realizar otra acción
            jumpState = JumpState.StartJump;
        else if (KeyUpSync) {                       // Hasta no confirmar que se ha detenido el salto, no realizar otra acción
            jumpState = JumpState.StopJumping;
        }
    }

    /// <summary>
    /// Comprueba si se está sprintando
    /// </summary>
    void checkSprint() {
        if (Input.GetKeyDown(KeyCode.LeftShift)) {
            pController.Sprinting(true);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift)) {
            pController.Sprinting(false);
        }
    }

    /// <summary>
    /// Cambia el control del movimiento del personaje
    /// </summary>
    public void changeControl() {
        whoHaveControl = (whoHaveControl == HaveControl.User) ? HaveControl.Code : HaveControl.User;
    }
}
