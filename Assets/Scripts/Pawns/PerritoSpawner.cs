﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla donde debe spawnear el enemigo */

public class PerritoSpawner : MonoBehaviour {

    /// <summary>
    /// Enemigo a spawnear
    /// </summary>
    PerritoController perritoController;

    void Start() {
        perritoController = GameObject.Find("EnemigoPerrito").GetComponent<PerritoController>();
    }

    /// <summary>
    /// Aparece cuando entra en cámara
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter2D(Collider2D col) {
        if (col.transform.tag == "MainCamera"){
            perritoController.transform.position = transform.position;
            perritoController.appearance(true);  
        }
    }

    /// <summary>
    /// Desaparece cuando sale de cámara
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerExit2D(Collider2D col) {
        if (col.transform.tag == "MainCamera") {
            perritoController.appearance(false);
        }
    }
}
