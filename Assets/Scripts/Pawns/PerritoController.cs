﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla a los enemigos inteligentes, los perritos */

public class PerritoController : MonoBehaviour {

    /// <summary>
    /// Comunicarse con el controlador del jugador
    /// </summary>
    PlayerController pController;

    /// <summary>
    /// Objeto que contiene las partículas con las que ataca
    /// </summary>
    public GameObject magicPower;

	// Use this for initialization
	void Start () {
        pController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<SpriteRenderer>().enabled) {
            Flip();
        }        
	}

    /// <summary>
    /// Gira el esqueleto del enemigo dependiendo de la posición del jugador
    /// </summary>
    void Flip() {
        if (pController.transform.position.x < transform.position.x){
            transform.GetChild(0).localScale = new Vector2(1, 1);
        } else {
            transform.GetChild(0).localScale = new Vector2(1, -1);
        }
    }

    /// <summary>
    /// Hace aparecer o desaparecer al enemigo
    /// </summary>
    /// <param name="appear">aparece el enemigo?</param>
    public void appearance(bool appear){
        GetComponent<SpriteRenderer>().enabled = appear;
        smokeEffect();

        if (appear) {
            InvokeRepeating("castAttack", 3f, 3f);
        } else {
            CancelInvoke("castAttack");
            GetComponent<Animator>().SetTrigger("Idle");
        }
    }

    /// <summary>
    /// Realiza un efecto de humo en la posición del enemigo
    /// </summary>
    void smokeEffect() {
        transform.GetChild(1).GetComponent<ParticleSystem>().Emit(30);
    }

    /// <summary>
    /// Prepara y calcula donde lanzar un ataque
    /// </summary>
    void castAttack() {
        GetComponent<Animator>().SetTrigger("Cast");

        RaycastHit2D hit = Physics2D.Raycast(new Vector2(pController.transform.position.x, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y), -Vector2.up, 100f, LayerMask.GetMask("Ground"));
        if (hit) {
            magicPower.transform.position = new Vector2(pController.transform.position.x, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
            magicPower.GetComponent<ParticleCollisionController>().destroyPoint = hit.point;
            Vector3 attackTarget = hit.point;
            StartCoroutine(attackPlayer(attackTarget));
        }
    }

    /// <summary>
    /// Lanza el ataque avisando con 1.5s de tiempo al usuario
    /// </summary>
    /// <param name="position">posición donde caerá el ataque</param>
    /// <returns>IEnumerator</returns>
    IEnumerator attackPlayer(Vector3 position) {
        GameObject indicator = GameObject.Find("AttackIndicator");
        indicator.transform.position = position;
        indicator.GetComponent<SpriteRenderer>().enabled = true;
        for (int i = 0; i < 5; i++){
            yield return new WaitForSeconds(0.3f);
            indicator.GetComponent<SpriteRenderer>().enabled = !indicator.GetComponent<SpriteRenderer>().enabled;
        }
        indicator.GetComponent<SpriteRenderer>().enabled = false;

        GetComponent<Animator>().SetTrigger("Attack");
        magicPower.GetComponent<ParticleSystem>().Emit(1);
    }
}
