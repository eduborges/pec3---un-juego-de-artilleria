﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla el comportamiento del bloque */

public class BlockController : MonoBehaviour {

    /// <summary>
    /// Cantidad de items de un tipo que contiene el bloque
    /// </summary>
    public int contentAmount = 1;

    /// <summary>
    /// Tipo de item que contiene el bloque
    /// </summary>
    public GameObject content;

    /// <summary>
    /// Puntos que otorga la destrucción de un bloque
    /// </summary>
    public int pointsValue = 10;
    
    /// <summary>
    /// Tiempo entre cada cambio de color del bloque
    /// </summary>
    public float colorDeltaTime = 0.5f;

    /// <summary>
    /// Colores por los que va a transitar el bloque
    /// </summary>
    public Color32[] colorsTransition;

    /// <summary>
    /// Índice para seleccionar el color con el que un bloque empieza a transitar en colorsTransition
    /// </summary>
    public int indexColor = 0;

    /// <summary>
    /// Tipos de bloque a elegir
    /// </summary>
    public enum BlockType { Interrogant, Indestructible, Brick}

    /// <summary>
    /// Tipo de bloque seleccionado
    /// </summary>
    public BlockType blockType;

    /// <summary>
    /// Collider en la parte inferior del bloque que es golpeada con la cabeza del personaje
    /// </summary>
    public BoxCollider2D blockBase;

    /// <summary>
    /// Collider en la parte superior del bloque con la que se golpean goomas y powerups
    /// </summary>
    public BoxCollider2D blockTop;

    /// <summary>
    /// Máscara para identificar si se ha golpeado a un enemigo con la parte superior del bloque
    /// </summary>
    public LayerMask whatIsEnemy;

    /// <summary>
    /// Máscara para identificar si se ha golpeado un powerUp con la parte superior del bloque
    /// </summary>
    public LayerMask whatIsPowerUp;

    /// <summary>
    /// Efecto de destrucción del bloque
    /// </summary>
    public GameObject prefabDestroyEffect;

    /// <summary>
    /// Objeto bloque que contendrá la mayor parte de la información
    /// </summary>
    Block block;

    /// <summary>
    /// Controlador del jugador para conocer el estado del mismo, ya que este influirá en las interacciones sobre los bloques
    /// </summary>
    PlayerController playerController;

    /// <summary>
    /// Asistente que indica que contenido debería tener los bloques con powerUp en cada momento
    /// </summary>
    ContentAssistant contentAssistant;

    // Inicialización del bloque
    void Start () {
        findPlayerController();
        findContentAssistant();
        blockDefinition();
    }
	
    /// <summary>
    /// Define el tipo de bloque en el que se transforma la clase base dependiendo de lo que se seleccione en el inspector
    /// </summary>
    void blockDefinition() {

        // Los bloques brick pueden tener contenido o no
        if (blockType == BlockType.Brick) {
            if (content != null)
                block = new BrickBlock(contentAmount, content);
            else
                block = new BrickBlock();
        }

        if (blockType == BlockType.Interrogant) {
            block = new InterrogationBlock(contentAmount, content);
        }

        GetComponent<SpriteRenderer>().sprite = block.aspect;

        // Si el bloque tiene la capacidad de cambiar de color, lo hará con la frecuencia definida en el inspector
        if (block.colorChanging)
            InvokeRepeating("changeColor", colorDeltaTime, colorDeltaTime);
    }

    /// <summary>
    /// Almacena el PlayerController del jugador en un atributo
    /// </summary>
    void findPlayerController() {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    void findContentAssistant() {
        contentAssistant = GameObject.Find("ContentAssistant").GetComponent<ContentAssistant>();
    }


        /// <summary>
        /// Acciones lanzadas cuando ocurre una colisión entre la cabeza del jugador y la base del bloque
        /// </summary>
        /// <param name="col">Detalles de la colision</param>
        void OnCollisionEnter2D(Collision2D col) {
        
        if (col.collider == playerController.headCollider && col.otherCollider == blockBase) {
            if(block.movable)                   // Si el bloque tiene la capacidad de moverse
                StartCoroutine(BlockUp());
            checkBlockHit();                    
        }
    }

    /// <summary>
    /// Acciones realizadas después de la colisión dependiendo de las características del personaje PlayerPower
    /// </summary>
    void checkBlockHit() {
        
        // Si el personaje está en la forma básicas sin powerups
        if (playerController.currentPower == PlayerController.PlayerPower.Basic) {
            if (block.canKillGooma) {
                checkBlockTop();
            }
        } else {
            if (block.destructible) {
                destroyBlock();
            }
        }
        
        // Si el bloque tiene contenido y no está vacío
        if (block.hasContent() && !block.empty()) {
            extractContent();
        }
    }

    /// <summary>
    /// Comprueba si hay algo en la parte superior del bloque en el momento del impacto
    /// </summary>
    void checkBlockTop() {
        Collider2D gotHittedByBlock;

        // Si hay un enemigo, este muere
        gotHittedByBlock = Physics2D.OverlapBox((Vector2)transform.position + blockTop.offset * transform.localScale.x, blockTop.size * transform.localScale.x, 0f, whatIsEnemy);
        if (gotHittedByBlock)
            StartCoroutine(gotHittedByBlock.gameObject.GetComponent<GoomaController>().killGooma(false));

        // Si hay un powerUp, este cambia el sentido de su movimiento
        gotHittedByBlock = Physics2D.OverlapBox((Vector2)transform.position + blockTop.offset * transform.localScale.x, blockTop.size * transform.localScale.x, 0f, whatIsPowerUp);
        if (gotHittedByBlock){
            if(gotHittedByBlock.tag == "Mushroom")
                gotHittedByBlock.gameObject.GetComponent<MushroomController>().Flip();
        }
            
    }

    /// <summary>
    /// Extrae el contenido del bloque
    /// </summary>
    void extractContent() {
        if(block.prefabContent.tag != "Coin")
            contentChangeCheck();

        popUpContent();            
        block.currentAmount--;

        // Si después de esta extracción el bloque queda vacío, este se transforma en uno indestructible
        if (block.empty()) {
            block = new IndestructibleBlock();
            GetComponent<SpriteRenderer>().sprite = block.aspect;

            if (!block.colorChanging) {
                CancelInvoke("changeColor");
                GetComponent<SpriteRenderer>().color = colorsTransition[0];
            }
        }
    }

    /// <summary>
    /// Instancia el contenido extraído del bloque y realiza su animación de aparición
    /// </summary>
    void popUpContent() {
        GameObject contentGO = Instantiate(block.prefabContent, transform);
        if(contentGO.GetComponent<CoinController>())
            StartCoroutine(contentGO.GetComponent<CoinController>().popUp());

        if(contentGO.GetComponent<MushroomController>())
            StartCoroutine(contentGO.GetComponent<MushroomController>().popUp());

        if (contentGO.GetComponent<FlowerController>())
            StartCoroutine(contentGO.GetComponent<FlowerController>().popUp());
    }

    /// <summary>
    /// Comprueba que el contenido del bloque sea adecuado para el poder actual del personaje, en caso negativo se modifica el contenido del bloque por el adecuado
    /// </summary>
    void contentChangeCheck() {

        GameObject contentRequired = contentAssistant.giveRequiredContent(playerController.currentPower);
        if (block.prefabContent.tag != contentRequired.tag) {
            block.changeContent(contentRequired);
        }
    }

    /// <summary>
    /// Ánimación de rebote del bloque
    /// </summary>
    /// <returns></returns>
    IEnumerator BlockUp() {

        float posY = 0.05f;
        for (int i = 0; i < 20; i++)
        {
            if (i == 10)
                posY = -posY;

            transform.position = new Vector3(transform.position.x, transform.position.y + posY, transform.position.z);
            yield return new WaitForSeconds(0.01f);
        }
    }

    /// <summary>
    /// Transita al siguiente color en la lista manejada como un vector circular
    /// </summary>
    void changeColor() {
        GetComponent<SpriteRenderer>().color = colorsTransition[indexColor];
        indexColor++;
        indexColor = indexColor % colorsTransition.Length;
    }

    /// <summary>
    /// Dibuja la parte superior de los bloques para facilitar la tarea de depurar
    /// </summary>
    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + (Vector3)blockTop.offset * transform.localScale.x, (Vector3)blockTop.size * transform.localScale.x);
    }


    /// <summary>
    /// Destruye el bloque, instancia el efecto de destrucción y suma los puntos del bloque
    /// </summary>
    void destroyBlock() {
        GameObject go = Instantiate(prefabDestroyEffect, transform);
        go.transform.SetParent(gameObject.transform.parent);
        GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>().AddPoints(pointsValue);
        AudioAgent.blockDestroy.GetComponent<AudioSource>().Play();
        Destroy(gameObject);
    }
}
