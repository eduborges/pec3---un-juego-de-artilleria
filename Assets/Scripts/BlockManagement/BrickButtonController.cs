﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla a los bloques que actúan como botones */

public class BrickButtonController : MonoBehaviour {

    /// <summary>
    /// ¿El bloque tiene el ratón encima? Necesario para no repetir continuamente las acciones de mouseOver
    /// </summary>
    bool selected = false;

    /// <summary>
    /// ¿Él bloque ya ha sido clickado?
    /// </summary>
    bool alreadyClicked = false;

    /// <summary>
    /// Tipos de acciones posibles a realizar
    /// </summary>
    public enum BrickButtonType { Play, Exit };

    /// <summary>
    /// Tipo de acción que realizará el bloque al ser clickado
    /// </summary>
    public BrickButtonType brickButtonType;

    /// <summary>
    /// Efecto de destrucción que tendrá este bloque al destruirse
    /// </summary>
    public GameObject DestroyParticles;

    // Comprobar en cada frame si ha sido clickado
    void Update() {
        isClicked();
    }

    /// <summary>
    /// Define los sonidos y animaciones a realizar dependiendo de si el cursor está sobre el bloque o no
    /// </summary>
    /// <param name="up">¿Está el ratón sobre el bloque?</param>
    public void mouseOver(bool up) {
        selected = up;
        GetComponent<Animator>().SetBool("isOver", up);
        if (up)
            AudioAgent.goomaKill.GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Comprueba si ha sido clickado el bloque
    /// </summary>
    void isClicked() {
        if (selected && Input.GetMouseButtonDown(0) && !alreadyClicked) {
            alreadyClicked = true;
            fakeDestroy();
            Instantiate(DestroyParticles,transform);
            AudioAgent.blockDestroy.GetComponent<AudioSource>().Play();
            Invoke("clickTransition",1f);
        }
    }

    /// <summary>
    /// Realiza la acción de la que se encarga ese bloque una vez es clickado
    /// </summary>
    void clickTransition() {
        if (brickButtonType == BrickButtonType.Play)           
            SceneAgent.newGame();
        else
            SceneAgent.exitGame();
    }

    /// <summary>
    /// Desactiva la visibilidad e interacción con el bloque
    /// </summary>
    void fakeDestroy() {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

}
