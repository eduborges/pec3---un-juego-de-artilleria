﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla el comportamiento de el efecto de destrucción de los bloques */

public class BlockDestroyer : MonoBehaviour {

    // Inicialización
	void Start () {
        forceEffect();
    }

    /// <summary>
    /// Destruye el objeto para liberar memoria y no se puedan acumular
    /// </summary>
    void destroyFragments(){
        Destroy(gameObject);
    }
	
    /// <summary>
    /// Aplica fuerzas a los fragmentos del bloque para dar un efecto real de destrucción
    /// </summary>
    void forceEffect() {
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 100f * (i + 1)));
        }

        Invoke("destroyFragments", 3f);
    }

}
